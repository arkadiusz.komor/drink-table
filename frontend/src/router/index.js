import Vue from 'vue'
import Router from 'vue-router'
import Employees from '@/components/Employees'
import AddEmployee from '@/components/AddEmployee'
import EditEmployee from '@/components/EditEmployee'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Employees',
      component: Employees
    },
    {
      path: '/add',
      name: 'addEmployee',
      component: AddEmployee
    },
    {
      path: '/:id/edit/',
      name: 'editEmployee',
      component: EditEmployee
    }
  ]
})
