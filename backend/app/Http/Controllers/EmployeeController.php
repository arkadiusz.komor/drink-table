<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use Validator;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Employee::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(),[
            'name' => 'required|string',
            'drink' => 'required|integer',
            'sugar' => 'required|integer',
            'milk' => 'required|integer',
        ]);
        if($validation->fails()){
            return response()->json(['status' => 0, 'error'=>$validation->errors()]);
        }


        Employee::create($request->all());
        return response()->json(['status'=> 1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Employee::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $validation = Validator::make($request->all(),[
            'name' => 'required|string',
            'drink' => 'required|integer',
            'sugar' => 'required|integer',
            'milk' => 'required|integer',
        ]);
        if($validation->fails()){
            return response()->json(['status' => 0, 'error'=>$validation->errors()]);
        }


        $employee->update($request->all());
        return response()->json(['status'=> 1]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        $employee->delete();
        return response()->json(['status'=> 1]);
    }
}
